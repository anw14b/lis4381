> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381 - Mobile Web App Development

## Andrew Whitely

### Assignment 3 Requirements:

1. create concert event app
2. have image, button on main screen
3. have different artists and prices
4. links to assignment files (.mwb and .sql)
5. provide screenshots

#### README.md file should include the following items:

* screenshots of app and erd

#### Assignment Screenshots:

*Screenshot of First screen*:

![First app screen](img/firstInterface.png)

*Screenshot of Second screen*:

![Second app screen](img/secondInterface.png)

*Screenshot of ERD*:

![ERD screenshot](img/a3_erd.png)


#### Links:

*.mwb file*
[A3.mwb](docs/a3.mwb "A3.mwb")

*.sql file*
[A3.sql](docs/a3.sql "A3.sql")

*Bitbucket Repo:*
[LIS 4381 Bitbucket Repo](https://bitbucket.org/anw14b/lis4381/ "LIS4381 Repo")

