> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381 - Mobile Web App Development

## Andrew Whitely

## Main README

1. [A1 README.md](a1/README.md)

    - install git, java, android Studio   
    - install ammpps
    - run localhost
    - basic git commands
    - screenshots of ammpps, android studio, java hello
    - bitbucket links
   
2. [A2 README.md](a2/README.md)

    - create healthy recipes application
    - use textviews
    - create classes

3. [A3 README.md](a3/README.md)
    
    - create ticket value app
    - create petstore, pet, and customer tables and present ERD
    - screenshots of application, ERD, as well as links to .mwb and .sql files
    
4. [P1 README.md](p1/README.md)
    
    - create business card app
    - screenshots of application, links

5. [A4 README.md](a4/README.md)
    
    - create pet store web app
    - screenshots of application included
    - links to web app locally
    - client side validation
    
6. [A5 README.md](a5/README.md)
    
    - create pet store web app
    - screenshots of application included
    - links to web app locally
    - server side validations
    
7. [P2 README.md](p2/README.md)
    - create pet store web app
    - screenshots of application included
    - links to web app locally
    - server side validations
    - edit and delete row functionality 

