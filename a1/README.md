> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381 - Mobile Web App Development

## Andrew Whitely

### Assignment 1 Requirements:

*Sub-Heading:*

1. Ordered-list items
2.
3.

#### README.md file should include the following items:

* Bullet-list items
*
*
*

> This is a blockquote.
>
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - initialize git repo
2. git status - check status of git repo
3. git add - add files to work index to be pushed
4. git commit - adds files to the commit line
5. git push - pushes files to repo
6. git pull - pulls changes from repo to local branch
7. git diff - checks differences in paths or files

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/ammpps.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/android.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/anw14b/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/anw14b/myteamquotes/ "My Team Quotes Tutorial")
