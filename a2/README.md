> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381 - Mobile Web App Development

## Andrew Whitely

### Assignment 2 Requirements:

1. create healthy recipes apps
2. have image, button on main screen
3. have ingredients and instructions on next screen after clicking button
4. extra credit: change background colors

#### README.md file should include the following items:

* screenshots of main screen and recipe screen


#### Assignment Screenshots:

*Screenshot of Main screen*:

![Main app screen](img/main.png)

*Screenshot of Recipe screen*:

![Recipe app screen](img/recipe.png)


#### Links:

*Bitbucket Repo:*
[LIS 4381 Bitbucket Repo](https://bitbucket.org/anw14b/lis4381/ "LIS4381 Repo")

