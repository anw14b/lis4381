> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381 - Mobile Web App Development

## Andrew Whitely

### Assignment 5 Requirements:

1. create mobile web app for petstore
2. have server-side data validation

#### README.md file should include the following items:

* screenshots of web app

#### Assignment Screenshots:

*Screenshot of valid info*:

![First app screen](img/first.png)

*Screenshot of form validation*:

![Second app screen](img/second.png)


#### Links:

*Bitbucket Repo:*
[LIS 4381 Bitbucket Repo](https://bitbucket.org/anw14b/lis4381/ "LIS4381 Repo")

*local repo*
[Local repo](http://localhost/repos/ "Local LIS4381 repo")