> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 4381 - Mobile Web App Development

## Andrew Whitely

### Project 1 Requirements:

1. create business card app
2. have image, button on main screen
3. have contact and interests on next screen after clicking button
4. change background colors

#### README.md file should include the following items:

* screenshots of main screen and contact screen


#### Assignment Screenshots:

*Screenshot of Main screen*:

![Main app screen](img/first.png)

*Screenshot of Recipe screen*:

![Second app screen](img/second.png)


#### Links:

*Bitbucket Repo:*
[LIS 4381 Bitbucket Repo](https://bitbucket.org/anw14b/lis4381/ "LIS4381 Repo")

